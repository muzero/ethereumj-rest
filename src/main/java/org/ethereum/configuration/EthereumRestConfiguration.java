
package org.ethereum.configuration;

import org.ethereum.facade.Ethereum;
import org.ethereum.facade.EthereumImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EthereumRestConfiguration {

    @Bean 
    public Ethereum ethereum() {
        return new EthereumImpl(); // singleton scope
    }

}