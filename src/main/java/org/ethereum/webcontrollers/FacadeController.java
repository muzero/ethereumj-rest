
package org.ethereum.webcontrollers;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.ethereum.core.Block;
import org.ethereum.facade.Ethereum;
import org.ethereum.response.ServiceResponse;

import static org.ethereum.response.ServiceResponseBuilder.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static org.springframework.util.Assert.*;

@Controller
@EnableAutoConfiguration
public class FacadeController {
    
    private static final Logger log = LoggerFactory.getLogger(FacadeController.class);
    
    @Autowired private Ethereum ethereum;
    
    @PostConstruct
    public void validateController() {
        notNull(ethereum);
    }
    
    @JsonSerialize protected static class BlockDto {
        @JsonProperty String hash;
        @JsonProperty String parentHash;
        @JsonProperty String uncleHash;
        @JsonProperty String coinbase;
        @JsonProperty String stateRoot;
        @JsonProperty String difficulty;
        @JsonProperty Long number;
        @JsonProperty Long minGasPrice;
        @JsonProperty Long gasLimit;
        @JsonProperty Long gasUsed;
        @JsonProperty Long timestamp;
        @JsonProperty String nonce;
    }
    
    @RequestMapping(value = "/block/{index}", method=RequestMethod.GET) 
    public @ResponseBody ServiceResponse<BlockDto> block(
            HttpServletRequest request,
            @PathVariable final Long index) {
        
        log.info("Retreiving block: {}");
        return success(dto(ethereum.getBlockByIndex(index)));
    }
  
    @RequestMapping("/blockchain") @ResponseBody
    public ServiceResponse<Long> blockchain() {
        log.info("Retreiving blockchainSize");
        return success(ethereum.getBlockChainSize());
    }
    
    @RequestMapping("/") @ResponseBody
    public String ping() {
        return "Ethereumj-rest-controller.";
    }

    private static BlockDto dto(final Block blockByIndex) {
        BlockDto blockDto = new BlockDto();
        blockDto.hash = str(blockByIndex.getHash());
        blockDto.parentHash = str(blockByIndex.getParentHash());
        blockDto.uncleHash = str(blockByIndex.getUnclesHash());
        blockDto.coinbase = str(blockByIndex.getCoinbase());
        
        blockDto.stateRoot = str(blockByIndex.getStateRoot());
        
        blockDto.difficulty = str(blockByIndex.getDifficulty());
        blockDto.number = blockByIndex.getNumber();
        blockDto.minGasPrice = blockByIndex.getMinGasPrice();
        blockDto.gasLimit = blockByIndex.getGasLimit();
        blockDto.gasUsed = blockByIndex.getGasUsed();
        
        blockDto.nonce = str(blockByIndex.getNonce());
        return blockDto;
    }
    
    private static String str(byte [] hash) {
        try {
            return new String(hash, "utf-8");
        } catch (UnsupportedEncodingException e) {
            log.error("unsupported Encoding", e);
        }
        return null;
    }
    

}