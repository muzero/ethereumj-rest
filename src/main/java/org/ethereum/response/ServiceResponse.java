package org.ethereum.response;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion;

/**
 * Service Response.
 * 
 * @param <T>
 */
@JsonSerialize
public class ServiceResponse<T> {
    @JsonProperty private Header header;
    @JsonProperty private T body;

    ServiceResponse(final Header header, final T body) {
        this.header = header;
        this.body = body;
    }
    
    ServiceResponse() {}

    public Header getHeader() {
        return header;
    }

    @JsonSerialize(include=Inclusion.NON_NULL)
    public T getBody() {
        return body;
    }

    public void setAccessToken(final String accessToken) {
        this.getHeader().setAccessToken(accessToken);
    }
    
    @JsonSerialize
    public static class Header {
        private boolean success;
        private Date dateTime;
        private Collection<String> errorCodes;

        @JsonSerialize(include=Inclusion.NON_NULL)
        private String accessToken;

        public Header(final boolean success, final Date dateTime,
                final List<String> errorCodes) {
            this.success = success;
            this.dateTime = new Date(dateTime.getTime());
            this.errorCodes = errorCodes;
        }

        public Header(final boolean success, final Date dateTime,
                final List<String> errorCodes, final String accessToken) {
            this.success = success;
            this.dateTime = new Date(dateTime.getTime());
            this.errorCodes = errorCodes;
            this.accessToken = accessToken;
        }
        
        public Header() {
        }

        public boolean isSuccess() {
            return success;
        }

        public String getDateTime() {
            return dateTime.toString();
        }

        public Collection<String> getErrorCodes() {
            return errorCodes;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setSuccess(final boolean success) {
            this.success = success;
        }

        public void setDateTime(final Date dateTime) {
            this.dateTime = new Date(dateTime.getTime());
        }

        public void setErrorCodes(final List<String> errorCodes) {
            this.errorCodes = errorCodes;
        }

        public void setAccessToken(final String accessToken) {
            this.accessToken = accessToken;
        }

        @Override
        public String toString() {
            return String
                    .format("Header [success=%s, errorCodes=%s, accessToken=%s, dateTime=%s]",
                            success, errorCodes, accessToken, dateTime);
        }
    }

    @Override
    public String toString() {
        return "ServiceResponse [header=" + header + ", body=" + body + "]";
    }
}
