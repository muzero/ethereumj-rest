package org.ethereum.response;


import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

/**
 * {@link ServiceResponse} Builder implementation
 */
public final class ServiceResponseBuilder {

    public static <T> ServiceResponse<T> success(final T body) {
        return new ServiceResponse<T>(buildSuccessHeader(), body);
    }

    public static <T> ServiceResponse<T> error(final String... errorMessage) {
        return new ServiceResponse<T>(buildErrorHeader(errorMessage), null);
    }
    
    public static <T> ServiceResponse<T> error(final Exception exception) {
        return new ServiceResponse<T>(buildErrorHeader(exception.getMessage()), null);
    }

    private static ServiceResponse.Header buildErrorHeader(final String... errorMessage) {
        return new ServiceResponse.Header(false, new Date(), Arrays.asList(errorMessage));
    }

    private static ServiceResponse.Header buildSuccessHeader() {
        return new ServiceResponse.Header(true, new Date(), Collections.<String>emptyList());
    }
}
